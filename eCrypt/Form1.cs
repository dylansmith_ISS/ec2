﻿using System;
using System.Text;
using System.Windows.Forms;
using System.IO;
using TeaTime;
using System.Runtime.InteropServices;

namespace eCrypt
{
	public partial class Form1 : Form
	{
		private string filename = "";
		private string formTitle = "eCrypt";
		private Encoding fileEncoding = Encoding.UTF8;


		public Form1()
		{
			InitializeComponent();
			this.Text = formTitle;


		}

		private bool validatePassword()
		{
			string pwd = PasswordTextBox.Text;
			string pwd2 = PasswordTextBoxConfirm.Text;

			if (pwd != pwd2)
			{
				MessageBox.Show("Passwords do not match!");
				return false;
			}
			else if (pwd.Length < 8 || pwd.Length > 16)
			{
				MessageBox.Show("Password Length must be between 8 and 16 characters long!");
				return false;
			}
			else return true;
		}

		private void OpenFileButton_Click(object sender, EventArgs e)
		{
			if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
			{
				filename = openFileDialog1.FileName;
				ContentsTextBox.Text = File.ReadAllText(filename, fileEncoding);

				this.Text = formTitle + " " + filename;
			}
		}

		private void EncryptButton_Click(object sender, EventArgs e)
		{
			// if (validatePassword())
			// {
			ContentsTextBox.Text = XXTEA.EncryptToBase64String(ContentsTextBox.Text, PasswordTextBox.Text);
			// }
		}

		private void DecryptButton_Click(object sender, EventArgs e)
		{
			// if (validatePassword())
			// {
			try
			{
				//test
				Convert.FromBase64String(ContentsTextBox.Text);
			}
			catch (Exception)
			{
				MessageBox.Show("Could Not Decrypt! file does not apear to be base64");
				return;
			}
			ContentsTextBox.Text = XXTEA.DecryptBase64StringToString(ContentsTextBox.Text, PasswordTextBox.Text);
			// }
		}

		private void SaveButton_Click(object sender, EventArgs e)
		{
			if (ContentsTextBox.Text.Length > 0)
			{
				if (saveFileDialog1.ShowDialog() == DialogResult.OK)
				{
					File.WriteAllText(saveFileDialog1.FileName, ContentsTextBox.Text, fileEncoding);
					this.Text = formTitle + " " + saveFileDialog1.FileName;
				}
			}
		}

		private void EncodingCheckBox_CheckedChanged(object sender, EventArgs e)
		{
			if (EncodingCheckBox.Checked)
			{
				fileEncoding = Encoding.GetEncoding(1252);
			}
			else fileEncoding = Encoding.UTF8;
		}
	}
}
